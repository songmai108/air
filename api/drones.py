from flask_injector import inject
from providers.DroneProvider import DroneProvider

@inject
def drone_request(data_provider: DroneProvider, cityDetailPayload: DroneProvider) -> str:
    return data_provider.drone_request(cityDetailPayload)

@inject
def total_drone_per_city(data_provider: DroneProvider, payload: DroneProvider) -> str:
    return data_provider.total_drone_per_city(payload)

@inject
def drone_monthly_rental_price(data_provider: DroneProvider, payload: DroneProvider) -> str:
    return data_provider.drone_monthly_rental_price(payload)    

@inject
def drone_rental_price_per_request(data_provider: DroneProvider, payload: DroneProvider) -> str:
    return data_provider.drone_rental_price_per_request(payload)        