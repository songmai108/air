from flask_injector import inject
from providers.CouchProvider import CouchProvider

@inject
def create_product(data_provider: CouchProvider, productPayload: CouchProvider) -> str:
    return data_provider.create_product(productPayload)

@inject
def read_product(data_provider: CouchProvider, prod_id: CouchProvider) -> str:
    return data_provider.read_product(prod_id)