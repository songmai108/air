from flask import Flask, request, jsonify
from flask_pymongo import PyMongo
import os
import json
import datetime 
import sys
import csv

from flask import current_app

app = Flask(__name__)

# add mongo url to flask config, so that flask_pymongo can use it to make connection
app.config['MONGO_URI'] = os.environ.get('DB')
mongo = PyMongo(app)

class DroneProvider(object):
    '''
    CRUD Functions on DB 
    '''
    def drone_request(self,payload):
        print('payload', payload, file=sys.stderr)
        if payload.get('cityname', None) is not None and payload.get('population', None) is not None and payload.get('geography', None) is not None and payload.get('calls', None) is not None:
            version = 'Version 0.0.1'
            pop1=payload['population']*0.394725393
            geo1=payload['geography']*0.918786978
            calls1=payload['calls']*-0.004728019
            
            indepVar = pop1+geo1+calls1

            coff = 0.000001111461
            intercept = 1334.252
            est_DR= indepVar * coff + intercept 

            city_data = {
            'cityname': payload['cityname'],
            'population': payload['population'],
            'geography_size': payload['geography'],
            'calls':payload['calls'],
            'estimated_drone_request': est_DR,
            'version': version
            }

            # db_response = mongo.db.drones.find_one({'cityname': payload['cityname']})
            # if(db_response is not None ):
            #     mongo.db.drones.updateOne(
            #         db_response[_id], {'$set': city_data}
            #         )
            # else:
            mongo.db.drones.insert_one(city_data)
            return jsonify({'status': True, 'estimate_drones': est_DR}), 200
        else:
            return jsonify({'status': False, 'message': 'Bad request parameters!'}), 400
