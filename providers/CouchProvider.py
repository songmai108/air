
from flask import Flask, request, jsonify
from flask_pymongo import PyMongo
import os
import json
import datetime 
import sys


from flask import current_app

app = Flask(__name__)

# add mongo url to flask config, so that flask_pymongo can use it to make connection
app.config['MONGO_URI'] = os.environ.get('DB')
mongo = PyMongo(app)


class CouchProvider(object):
    '''
    CRUD Functions on DB 
    '''

    def create_product(self,payload):
        print('payload', payload, file=sys.stderr)

        if payload.get('_id', None) is not None :
            mongo.db.products.insert_one(payload)
            return jsonify({'ok': True, 'message': 'User created successfully!'}), 200
        else:
            return jsonify({'ok': False, 'message': 'Bad request parameters!'}), 400
          
    def read_product(self, prod_id) -> str:

        product = mongo.db.products.find_one(prod_id)
        return jsonify(product), 200

    def delete_product(self, payload) -> str:

        if payload.get('_id', None) is not None :
            db_response = mongo.db.users.delete_one({'_id': payload['_id']})
            if db_response.deleted_count == 1:
                response = {'ok': True, 'message': 'record deleted'}
            else:
                response = {'ok': True, 'message': 'no record found'}
            return jsonify(response), 200
        else:
            return jsonify({'ok': False, 'message': 'Bad request parameters!'}), 400 

    def update_product(self, payload) -> str:
        if payload.get('_id', {}) != {}:
            mongo.db.users.update_one(
                payload['_id'], {'$set': payload.get('payload', {})})
            return jsonify({'ok': True, 'message': 'record updated'}), 200
        else:
            return jsonify({'ok': False, 'message': 'Bad request parameters!'}), 400            